var React = require('react');
var ReactDOM = require('react-dom');
var CSSTransitionGroup = require('react-addons-css-transition-group');

var App = React.createClass({
	getInitialState: function(){
		return{
			today: [
				"123123123",
				"123123123",
				"123123123",
				"123123123",
				"123123123",
				"123123123",
				"123123123",
				"123123123",
				"123123123",
				"123123123",
				"123123123",
				"123123123",
				"123123123",
				"123123123",
				"123123123",
				"123123123",
				"123123123",
				"123123123",
				"123123123"
			],
			previousSix: [
				20,
				10,
				10,
				20,
				15,
				10
			],
			recordHigh: 65,
			allTimeTotal: 518,
			dailyAverage: 25,
			weeklyAverage: 804
		}
	},
	handleMessageSubmit: function(message){
		this.setState({
			text: message
		})
	},
	render: function(){
		return(
			<div className="coffee-app">
				<Header />
				<TotalBox total={ this.state.allTimeTotal } />
				<CupsSlider
					today={ this.state.today.length }
					record={ this.state.recordHigh } />
				<CoffeeStats 
					dailyAverage={ this.state.dailyAverage }
					weeklyAverage={ this.state.weeklyAverage }/>
				<AnimatedCup 
					todayAnimate={ this.state.today.length }/>
				<WeeklyGraph
					previousSix={ this.state.previousSix }
					today={ this.state.today.length }/>
				<MessageForm onMessageSubmit={this.handleMessageSubmit}/>
			</div>
		)
	}
});

var Header = React.createClass({
	render: function(){
		return(
			<header>
				<div className="header-container">
					<div className="header-decoration"></div>
					<CSSTransitionGroup
						component="span"
						transitionName="header"
						transitionAppear={true}
						transitionAppearTimeout={5}
						transitionEnterTimeout={5000}
						transitionLeaveTimeout={5000}
					>
						<h1 className="header-text">Manifest Coffee</h1>
					</CSSTransitionGroup>
				</div>
			</header>
		)
	}
});

var TotalBox = React.createClass({
	render: function(){
		return(
			<div className="total-box">
				<h2>All Time Total</h2>
				<h3>{ this.props.total }</h3>
			</div>
		)
	}
});

var CupsSlider = React.createClass({
	render: function(){
			var todayCups = this.props.today;
			var recordCups = this.props.record;
			var cupsMessage = 'Most Cups in a Day: '
			var sliderWidth;
			if (todayCups >= recordCups){
				recordCups = todayCups;
				cupsMessage = 'New Record Cups in a Day: ';
			}
			if (todayCups === 1 ){
				sliderWidth = 16;
			}
			else{
				sliderWidth = parseInt( (todayCups / recordCups) * 720 );
			}
		return(
			<div className="cup-slider-bar">
				<svg id="cups-slider" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 768 76">
				  <rect className="slider-body" width="768" height="76" rx="12" ry="12"/>
				  <rect className="slider-bar" x="25" y="15" width="720" height="16" rx="8" ry="8"/>
				  <rect className="slider-data" id="slider-data" x="25" y="15" width={sliderWidth} height="16" rx="8" ry="8"/>
				  {/*<text className="slider-text" transform="translate(26 60)"><tspan>Cups Today: </tspan><tspan>{todayCups}</tspan></text>
				  <text className="slider-text" transform="translate(530 60)">{cupsMessage + recordCups}</text>*/}
				  <line className="slider-dashed-line" x1="730" y1="58" x2="730" y2="0"/>
				</svg>
				<div className="slider-text header-left">
					<h2>Cups Today:&nbsp;<span className="number-wrapper">
						<CSSTransitionGroup className="today-span" component="span" transitionName="cups-slider" transitionEnterTimeout={5000} transitionLeaveTimeout={5000}>
							<span key={todayCups}>{todayCups}</span>
						</CSSTransitionGroup>
						</span>
					</h2>
				</div>
				<div className="slider-text header-right">
					<h2>{cupsMessage + recordCups}</h2>
				</div>
			</div>
		)
	}
});

var CoffeeStats = React.createClass({
	render: function(){
		return(
			<div className="coffee-stats">
				<h2>Coffee Stats</h2>
				<ul>
					<li>{this.props.dailyAverage} cups a day</li>
					<li>{this.props.weeklyAverage} cups a week</li>
					<li>Monday averages highest consumption</li>
				</ul>
			</div>
		)
	}
});

var AnimatedCup = React.createClass({
	componentWillUpdate: function() {
	    ReactDOM.findDOMNode(this).classList.add("animate-cup");
	},
	componentDidUpdate: function() {
		var component = ReactDOM.findDOMNode(this);
		setTimeout(function(){
			component.classList.remove("animate-cup");
		}, 30000);
	},
	render: function(){
		return(
			<div className="cup-container">
				<svg id="cup-image" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 255.98 220">
					<rect className="fill-rectangle" x="93.35" y="24.9" width="144" height="172" rx="4" ry="4"/>
					<rect className="matched-rectangle" x="93.35" y="24.9" width="144" height="172" data={this.props.todayAnimate}/>
					<path className="cup-body" d="M255.6,8.78A8.75,8.75,0,0,0,247,0H80.81A8.76,8.76,0,0,0,72.2,8.78c0,9.11-.2,19.74-0.34,31.34C49.11,12.71,5.18,22.84.63,58.68c-3.31,26.07,6.62,54.13,23.16,74.07,10.45,12.6,30.87,23.62,43.9,32.72C75,170.59,83,177.11,89.08,184.24c2.6,10.5,5.17,20,7.39,27.24,1.6,5.26,3.87,8.53,8.61,8.53H226.31c4.73,0,7.46-3.93,8.61-8.52,6.58-26.35,16.08-75,18.17-100.27C257.64,56.44,255.41,34.38,255.6,8.78ZM61.22,137.27c-7.6-4.66-23.26-17.47-28.43-24.66-8.22-11.44-14.9-30.68-13.71-45.46C20.78,46,41.51,35.69,57.5,53.5,65.71,62.64,61.13,90,73,94.11c0.37,5.31.84,10.69,1.43,16.14,1.13,10.37,3.77,25.15,7,40.68A200.57,200.57,0,0,0,61.22,137.27Zm172.93-26.19c-1.63,19.63-9,57.57-14.11,78-0.89,3.57-3,6.88-6.68,6.88H119.22c-3.68,0-5.44-2.8-6.68-6.88-5.87-19.25-15-59.18-17.08-78.77-3.34-30.73-1.67-59-1.76-78.67A6.69,6.69,0,0,1,100.37,25h129a6.64,6.64,0,0,1,6.68,6.63C236,51.52,237.68,68.57,234.15,111.09Z"/>
				</svg>
			</div>
		)
	}
});
var WeeklyGraph = React.createClass({
	render: function(){
		function normalizeArray(inputArray){
			var statArr = inputArray.slice();
			var maxNum = scaleTop;

			for (var i = statArr.length - 1; i >= 0; i--) {
			  var curNum = statArr[i];
			  statArr[i] = Math.round( 220 - ( 156 * ((curNum) / maxNum) ) );
			}
			return statArr;
		}
		function textValueReduce(inputArray){
			var textValueArray = inputArray.slice();

			for (var i = textValueArray.length -1; i >= 0; i--){
				var curNum = textValueArray[i]
				textValueArray[i] = curNum + 7;
			}
			return textValueArray;
		}

		var todayCount = this.props.today;
		var previousSix = this.props.previousSix.slice();
		
		var addToday = previousSix.unshift(todayCount);
		var arrayHighest = Math.max.apply( Math, previousSix);
		var scaleTop = (Math.ceil(arrayHighest / 10) * 10) + 5;

		var graphArray = normalizeArray(previousSix);
		var textArray = textValueReduce(graphArray);

		var polylinePoints = '100,' + graphArray[6] + ' 200,' + graphArray[5] + ' 300,' + graphArray[4] + ' 400,' + graphArray[3] + ' 500,' + graphArray[2] + ' 600,' + graphArray[1] + ' 700,'+ graphArray[0];
		return(
			<div className="weekly-graph">
				<svg id="weekly-data" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 299.68">
				  <title>Weekly Data Chart</title>
				  <rect className="chart-body" width="800" height="299.68" rx="12" ry="12"/>
				  <path className="date-bar" d="M800,240.76v46.92a12.29,12.29,0,0,1-12.49,12H12.91a12.29,12.29,0,0,1-12.49-12V240.76"/>

				  <line className="vertical-lines" x1="100" y1="220" x2="100" y2={graphArray[6]}/>
				  <line className="vertical-lines" x1="200" y1="220" x2="200" y2={graphArray[5]}/>
				  <line className="vertical-lines" x1="300" y1="220" x2="300" y2={graphArray[4]}/>
				  <line className="vertical-lines" x1="400" y1="220" x2="400" y2={graphArray[3]}/>
				  <line className="vertical-lines" x1="500" y1="220" x2="500" y2={graphArray[2]}/>
				  <line className="vertical-lines" x1="600" y1="220" x2="600" y2={graphArray[1]}/>
				  <line className="vertical-lines" x1="700" y1="220" x2="700" y2={graphArray[0]}/>
				  <line className="dashed-line" x1="100" y1="64" x2="700" y2="64"/>

				  <polyline className="chart-line" points={polylinePoints} />


					<g className="data-dot">
						<circle className="data-dot-outer" cx="100" cy={graphArray[6]} r="11"/>
						<circle className="data-dot-inner" cx="100" cy={graphArray[6]} r="7"/>
						<text className="data-dot-value" textAnchor="middle" transform={"translate(100 " + textArray[6] + ")" }>{ previousSix[6] }</text>
					</g>
					
					<g className="data-dot">
					  <circle className="data-dot-outer" cx="200" cy={graphArray[5]} r="11"/>
					  <circle className="data-dot-inner" cx="200" cy={graphArray[5]} r="7"/>
					  <text className="data-dot-value" textAnchor="middle" transform={"translate(200 " + textArray[5] + ")" }>{ previousSix[5] }</text>
					</g>

					<g className="data-dot">
					  <circle className="data-dot-outer" cx="300" cy={graphArray[4]} r="11"/>
					  <circle className="data-dot-inner" cx="300" cy={graphArray[4]} r="7"/>
					  <text className="data-dot-value" textAnchor="middle" transform={"translate(300 " + textArray[4] + ")" }>{ previousSix[4] }</text>
					</g>

					<g className="data-dot">
					  <circle className="data-dot-outer" cx="400" cy={graphArray[3]} r="11"/>
					  <circle className="data-dot-inner" cx="400" cy={graphArray[3]} r="7"/>
					  <text className="data-dot-value" textAnchor="middle" transform={"translate(400 " + textArray[3] + ")" }>{ previousSix[3] }</text>
					</g>

					<g className="data-dot">
					  <circle className="data-dot-outer" cx="500" cy={graphArray[2]} r="11"/>
					  <circle className="data-dot-inner" cx="500" cy={graphArray[2]} r="7"/>
					  <text className="data-dot-value" textAnchor="middle" transform={"translate(500 " + textArray[2] + ")" }>{ previousSix[2] }</text>
					</g>

					<g className="data-dot">
					  <circle className="data-dot-outer" cx="600" cy={graphArray[1]} r="11"/>
					  <circle className="data-dot-inner" cx="600" cy={graphArray[1]} r="7"/>
					  <text className="data-dot-value" textAnchor="middle" transform={"translate(600 " + textArray[1] + ")" }>{ previousSix[1] }</text>
					</g>

					<g className="data-dot">
					  <circle className="data-dot-outer" cx="700" cy={graphArray[0]} r="11"/>
					  <circle className="data-dot-inner" cx="700" cy={graphArray[0]} r="7"/>
					  <text className="data-dot-value" textAnchor="middle" transform={"translate(700 " + textArray[0] + ")" }>{ previousSix[0] }</text>
					</g>



				  
				  <text className="x-label" transform="translate(85 275)">4/7</text>
				  <text className="x-label" transform="translate(185 275)">4/8</text>
				  <text className="x-label" transform="translate(285 275)">4/9</text>
				  <text className="x-label" transform="translate(385 275)">4/10</text>
				  <text className="x-label" transform="translate(485 275)">4/11</text>
				  <text className="x-label" transform="translate(585 275)">4/12</text>
				  <text className="x-label" transform="translate(685 275)">4/13</text>
				  <text className="y-label" transform="translate(42 71)">{ scaleTop }</text>
				  <text className="y-label" transform="translate(42 226)">0</text>
				  <text className="chart-title" transform="translate(299.08 44.82)">Weekly Data</text>
				  <line className="decorative-line" x1="800" y1="245.82" y2="245.82"/>
				</svg>
			</div>
		)
	}
});

var MessageForm = React.createClass({
	getInitialState: function(){
		return { text: ''};
	},
	handleTextChange: function(e){
		this.setState({text : e.target.value});
	},
	handleSubmit: function(e){
		e.preventDefault();
		var text = this.state.text.trim();
		if (!text){
			return;
		}
		this.props.onMessageSubmit({text: text});
		this.setState({text: ''});
	},
	render: function() {
		return (
			<form className="messageForm" onSubmit={this.handleSubmit}>
				<input
					type="text"
					placeholder="Say something..."
					value={this.state.text}
					onChange={this.handleTextChange}
				/>
				<input type="submit" value="Send Message" />
			</form>
		)
	}
});

ReactDOM.render(
	<App />,
	document.getElementById('main')
);