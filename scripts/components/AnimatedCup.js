/*
	<AnimatedCup />
*/

import React from 'react';

var AnimatedCup = React.createClass({
	componentWillUpdate: function() {
	    ReactDOM.findDOMNode(this).classList.add("animate-cup");
	},
	componentDidUpdate: function() {
		var component = ReactDOM.findDOMNode(this);
		setTimeout(function(){
			component.classList.remove("animate-cup");
		}, 5000);
	},
	render: function(){
		return(
			<div className="cup-container">
				<svg id="cup-image" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 255.98 220">
					<rect className="fill-rectangle" x="93.35" y="24.9" width="144" height="172" rx="4" ry="4"/>
					<rect className="matched-rectangle" x="93.35" y="24.9" width="144" height="172" data={this.props.todayAnimate}/>
					<path className="cup-body" d="M255.6,8.78A8.75,8.75,0,0,0,247,0H80.81A8.76,8.76,0,0,0,72.2,8.78c0,9.11-.2,19.74-0.34,31.34C49.11,12.71,5.18,22.84.63,58.68c-3.31,26.07,6.62,54.13,23.16,74.07,10.45,12.6,30.87,23.62,43.9,32.72C75,170.59,83,177.11,89.08,184.24c2.6,10.5,5.17,20,7.39,27.24,1.6,5.26,3.87,8.53,8.61,8.53H226.31c4.73,0,7.46-3.93,8.61-8.52,6.58-26.35,16.08-75,18.17-100.27C257.64,56.44,255.41,34.38,255.6,8.78ZM61.22,137.27c-7.6-4.66-23.26-17.47-28.43-24.66-8.22-11.44-14.9-30.68-13.71-45.46C20.78,46,41.51,35.69,57.5,53.5,65.71,62.64,61.13,90,73,94.11c0.37,5.31.84,10.69,1.43,16.14,1.13,10.37,3.77,25.15,7,40.68A200.57,200.57,0,0,0,61.22,137.27Zm172.93-26.19c-1.63,19.63-9,57.57-14.11,78-0.89,3.57-3,6.88-6.68,6.88H119.22c-3.68,0-5.44-2.8-6.68-6.88-5.87-19.25-15-59.18-17.08-78.77-3.34-30.73-1.67-59-1.76-78.67A6.69,6.69,0,0,1,100.37,25h129a6.64,6.64,0,0,1,6.68,6.63C236,51.52,237.68,68.57,234.15,111.09Z"/>
				</svg>
			</div>
		)
	}
});

export default AnimatedCup;