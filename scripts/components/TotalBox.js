/*
	<TotalBox/>
*/

import React from 'react';

var TotalBox = React.createClass({
	render: function(){
		return(
			<div className="total-box">
				<h2>All Time Total</h2>
				<h3>{ this.props.total }</h3>
			</div>
		)
	}
});

export default TotalBox;