/*
	<CoffeeStats />
*/

import React from 'react';

var CoffeeStats = React.createClass({
	render: function(){
		return(
			<div className="coffee-stats">
				<h2>Coffee Stats</h2>
				<ul>
					<li>{this.props.dailyAverage} cups a day</li>
					<li>{this.props.weeklyAverage} cups a week</li>
					<li>Monday averages highest consumption</li>
				</ul>
			</div>
		)
	}
});

export default CoffeeStats;