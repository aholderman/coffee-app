/*
  App
*/

import React from 'react';

import Header from './components/Header';
import TotalBox from './components/TotalBox';
import CupsSlider from './components/CupsSlider';
import CoffeeStats from './components/CoffeeStats';
import WeeklyGraph from './components/WeeklyGraph';
import MessageForm from './components/MessageForm';

var App = React.createClass({
  getInitialState: function(){
    return{
      today: [
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123",
        "123123123"
      ],
      previousSix: [
        20,
        55,
        10,
        20,
        95,
        10
      ],
      recordHigh: 65,
      allTimeTotal: 518,
      dailyAverage: 25,
      weeklyAverage: 804
    }
  },
  handleMessageSubmit: function(message){
    this.setState({
      text: message
    })
  },
  render: function(){
    return(
      <div className="coffee-app">
        <Header />
        <TotalBox total={ this.state.allTimeTotal } />
        <CupsSlider
          today={ this.state.today.length }
          record={ this.state.recordHigh } />
        <CoffeeStats 
          dailyAverage={ this.state.dailyAverage }
          weeklyAverage={ this.state.weeklyAverage }/>
        <AnimatedCup 
          todayAnimate={ this.state.today.length }/>
        <WeeklyGraph
          previousSix={ this.state.previousSix }
          today={ this.state.today.length }/>
        <MessageForm onMessageSubmit={this.handleMessageSubmit}/>
      </div>
    )
  }
});


export default App;