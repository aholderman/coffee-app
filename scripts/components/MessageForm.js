/*
	<MessageForm />
*/

import React from 'react';

var MessageForm = React.createClass({
	getInitialState: function(){
		return { text: ''};
	},
	handleTextChange: function(e){
		this.setState({text : e.target.value});
	},
	handleSubmit: function(e){
		e.preventDefault();
		var text = this.state.text.trim();
		if (!text){
			return;
		}
		this.props.onMessageSubmit({text: text});
		this.setState({text: ''});
	},
	render: function() {
		return (
			<form className="messageForm" onSubmit={this.handleSubmit}>
				<input
					type="text"
					placeholder="Say something..."
					value={this.state.text}
					onChange={this.handleTextChange}
				/>
				<input type="submit" value="Send Message" />
			</form>
		)
	}
});

export default MessageForm;