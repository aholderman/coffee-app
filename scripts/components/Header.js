/*
	<Header />
*/

import React from 'react';

var Header = React.createClass({
	render: function(){
		return(
			<header>
				<div className="header-container">
					<div className="header-decoration"></div>
					<CSSTransitionGroup
						component="span"
						transitionName="header"
						transitionAppear={true}
						transitionAppearTimeout={5}
						transitionEnterTimeout={5000}
						transitionLeaveTimeout={5000}
					>
						<h1 className="header-text">Manifest Coffee</h1>
					</CSSTransitionGroup>
				</div>
			</header>
		)
	}
});

export default Header;