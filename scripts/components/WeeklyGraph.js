/*
	<WeeklyGraph />
*/

import React from 'react';

var WeeklyGraph = React.createClass({
	render: function(){
		function normalizeArray(inputArray){
			var statArr = inputArray.slice();
			var maxNum = scaleTop;

			for (var i = statArr.length - 1; i >= 0; i--) {
			  var curNum = statArr[i];
			  statArr[i] = Math.round( 220 - ( 156 * ((curNum) / maxNum) ) );
			}
			return statArr;
		}
		function textValueReduce(inputArray){
			var textValueArray = inputArray.slice();

			for (var i = textValueArray.length -1; i >= 0; i--){
				var curNum = textValueArray[i]
				textValueArray[i] = curNum + 7;
			}
			return textValueArray;
		}
		var arrayHighest = Math.max.apply( Math, this.props.previousSix);
		var scaleTop = (Math.ceil(arrayHighest / 10) * 10) + 5;

		var todayCount = this.props.today;
		var previousSix = this.props.previousSix.slice();
		var addToday = previousSix.unshift(todayCount);
		var graphArray = normalizeArray(previousSix);
		var textArray = textValueReduce(graphArray);
		var polylinePoints = '100,' + graphArray[6] + ' 200,' + graphArray[5] + ' 300,' + graphArray[4] + ' 400,' + graphArray[3] + ' 500,' + graphArray[2] + ' 600,' + graphArray[1] + ' 700,'+ graphArray[0];
		return(
			<div className="weekly-graph">
				<svg id="weekly-data" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 299.68">
				  <title>Weekly Data Chart</title>
				  <rect className="chart-body" width="800" height="299.68" rx="12" ry="12"/>
				  <path className="date-bar" d="M800,240.76v46.92a12.29,12.29,0,0,1-12.49,12H12.91a12.29,12.29,0,0,1-12.49-12V240.76"/>

				  <line className="vertical-lines" x1="100" y1="220" x2="100" y2={graphArray[6]}/>
				  <line className="vertical-lines" x1="200" y1="220" x2="200" y2={graphArray[5]}/>
				  <line className="vertical-lines" x1="300" y1="220" x2="300" y2={graphArray[4]}/>
				  <line className="vertical-lines" x1="400" y1="220" x2="400" y2={graphArray[3]}/>
				  <line className="vertical-lines" x1="500" y1="220" x2="500" y2={graphArray[2]}/>
				  <line className="vertical-lines" x1="600" y1="220" x2="600" y2={graphArray[1]}/>
				  <line className="vertical-lines" x1="700" y1="220" x2="700" y2={graphArray[0]}/>
				  <line className="dashed-line" x1="100" y1="64" x2="700" y2="64"/>

				  <polyline className="chart-line" points={polylinePoints} />

					<g className="data-dot">
						<circle className="data-dot-outer" cx="100" cy={graphArray[6]} r="11"/>
						<circle className="data-dot-inner" cx="100" cy={graphArray[6]} r="7"/>
						<text className="data-dot-value" textAnchor="middle" transform={"translate(100 " + textArray[6] + ")" }>{ previousSix[6] }</text>
					</g>
					
					<g className="data-dot">
					  <circle className="data-dot-outer" cx="200" cy={graphArray[5]} r="11"/>
					  <circle className="data-dot-inner" cx="200" cy={graphArray[5]} r="7"/>
					  <text className="data-dot-value" textAnchor="middle" transform={"translate(200 " + textArray[5] + ")" }>{ previousSix[5] }</text>
					</g>

					<g className="data-dot">
					  <circle className="data-dot-outer" cx="300" cy={graphArray[4]} r="11"/>
					  <circle className="data-dot-inner" cx="300" cy={graphArray[4]} r="7"/>
					  <text className="data-dot-value" textAnchor="middle" transform={"translate(300 " + textArray[4] + ")" }>{ previousSix[4] }</text>
					</g>

					<g className="data-dot">
					  <circle className="data-dot-outer" cx="400" cy={graphArray[3]} r="11"/>
					  <circle className="data-dot-inner" cx="400" cy={graphArray[3]} r="7"/>
					  <text className="data-dot-value" textAnchor="middle" transform={"translate(400 " + textArray[3] + ")" }>{ previousSix[3] }</text>
					</g>

					<g className="data-dot">
					  <circle className="data-dot-outer" cx="500" cy={graphArray[2]} r="11"/>
					  <circle className="data-dot-inner" cx="500" cy={graphArray[2]} r="7"/>
					  <text className="data-dot-value" textAnchor="middle" transform={"translate(500 " + textArray[2] + ")" }>{ previousSix[2] }</text>
					</g>

					<g className="data-dot">
					  <circle className="data-dot-outer" cx="600" cy={graphArray[1]} r="11"/>
					  <circle className="data-dot-inner" cx="600" cy={graphArray[1]} r="7"/>
					  <text className="data-dot-value" textAnchor="middle" transform={"translate(600 " + textArray[1] + ")" }>{ previousSix[1] }</text>
					</g>

					<g className="data-dot">
					  <circle className="data-dot-outer" cx="700" cy={graphArray[0]} r="11"/>
					  <circle className="data-dot-inner" cx="700" cy={graphArray[0]} r="7"/>
					  <text className="data-dot-value" textAnchor="middle" transform={"translate(700 " + textArray[0] + ")" }>{ previousSix[0] }</text>
					</g>



				  
				  <text className="x-label" transform="translate(85 275)">4/7</text>
				  <text className="x-label" transform="translate(185 275)">4/8</text>
				  <text className="x-label" transform="translate(285 275)">4/9</text>
				  <text className="x-label" transform="translate(385 275)">4/10</text>
				  <text className="x-label" transform="translate(485 275)">4/11</text>
				  <text className="x-label" transform="translate(585 275)">4/12</text>
				  <text className="x-label" transform="translate(685 275)">4/13</text>
				  <text className="y-label" transform="translate(42 71)">{ scaleTop }</text>
				  <text className="y-label" transform="translate(42 226)">0</text>
				  <text className="chart-title" transform="translate(299.08 44.82)">Weekly Data</text>
				  <line className="decorative-line" x1="800" y1="245.82" y2="245.82"/>
				</svg>
			</div>
		)
	}
});

export default WeeklyGraph;