/*
	<CupsSlider />
*/

import React from 'react';

var CupsSlider = React.createClass({
	render: function(){
			var todayCups = this.props.today;
			var recordCups = this.props.record;
			var cupsMessage = 'Most Cups in a Day: '
			var sliderWidth;
			if (todayCups >= recordCups){
				recordCups = todayCups;
				cupsMessage = 'New Record Cups in a Day: ';
			}
			if (todayCups === 1 ){
				sliderWidth = 16;
			}
			else{
				sliderWidth = parseInt( (todayCups / recordCups) * 720 );
			}
		return(
			<div className="cup-slider-bar">
				<svg id="cups-slider" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 768 76">
				  <rect className="slider-body" width="768" height="76" rx="12" ry="12"/>
				  <rect className="slider-bar" x="25" y="15" width="720" height="16" rx="8" ry="8"/>
				  <rect className="slider-data" id="slider-data" x="25" y="15" width={sliderWidth} height="16" rx="8" ry="8"/>
				  <text className="slider-text" transform="translate(26 60)">{'Cups Today: ' + todayCups}</text>
				  <text className="slider-text" transform="translate(530 60)">{cupsMessage + recordCups}</text>
				  <line className="slider-dashed-line" x1="730" y1="60" x2="730" y2="0"/>
				</svg>
			</div>
		)
	}
});

export default CupsSlider;